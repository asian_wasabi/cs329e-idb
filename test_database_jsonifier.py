from database_jsonifier import convertNumToExcelColumnForm, getBands, getSongs, getAlbums
import os
from unittest import TestCase, main
import openpyxl

#10 tests for convertNumToExcelColumnForm()
#3 tests for getBands()
#3 tests for getAlbums()
#3 tests for getSongs()

class database_jsonifier_tests(TestCase):
    # testing convertNumToExcelColumnForm()
    # test results come from: https://www.dcode.fr/base-26-cipher
    # go to the decoder/converter, type in a number, check the second
    # option under value for A, and then decrypt
    def test_convertNumToExcelColumnForm_1(self):
        self.assertEqual(convertNumToExcelColumnForm(18), "R")

    def test_convertNumToExcelColumnForm_2(self):
        self.assertEqual(convertNumToExcelColumnForm(27), "AA")

    def test_convertNumToExcelColumnForm_3(self):
        self.assertEqual(convertNumToExcelColumnForm(4959), "GHS")

    def test_convertNumToExcelColumnForm_4(self):
        self.assertEqual(convertNumToExcelColumnForm(42), "AP")

    def test_convertNumToExcelColumnForm_5(self):
        self.assertEqual(convertNumToExcelColumnForm(2204), "CFT")

    def test_convertNumToExcelColumnForm_6(self):
        self.assertEqual(convertNumToExcelColumnForm(1234), "AUL")

    def test_convertNumToExcelColumnForm_7(self):
        self.assertEqual(convertNumToExcelColumnForm(6190), "IDB")

    def test_convertNumToExcelColumnForm_8(self):
        self.assertEqual(convertNumToExcelColumnForm(8271), "LFC")

    def test_convertNumToExcelColumnForm_9(self):
        self.assertEqual(convertNumToExcelColumnForm(1189), "ASS")

    def test_convertNumToExcelColumnForm_10(self):
        self.assertEqual(convertNumToExcelColumnForm(4145), "FCK")

    # testing getBands()
    def test_getBands_1(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Band Name"
        ws['B1'].value = "Place of Origin"
        ws['C1'].value = "Year Started"
        ws['D1'].value = "Genre"
        ws['E1'].value = "Website"
        for i in range(1, 11):
            ws[chr(69+i)+'1'].value = "Album "+str(i)
        for i in range(1, 11):
            ws[chr(79+i)+'1'].value = "Notable Song "+str(i)
        ws['Z1'].value = "Member 1"
        for i in range(2, 11):
            ws['A'+chr(63+i)+'1'].value = "Member "+str(i)
        ws['AJ1'].value = "Image"

        ws['A2'].value = "Arctic Monkeys"
        ws['B2'].value = "Sheffield, England"
        ws['C2'].value = "2002"
        ws['D2'].value = "Rock"
        ws['E2'].value = "https://www.arcticmonkeys.com/"
        ws['F2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['G2'].value = "Favourite Worst Nightmare"
        ws['H2'].value = "Humbug"
        ws['I2'].value = "Suck It and See"
        ws['J2'].value = "AM"
        ws['K2'].value = "Tranquility Base Hotel and Casino"
        ws['P2'].value = "Brianstorm"
        ws['Q2'].value = "Star Treatment"
        ws['R2'].value = "505"
        ws['S2'].value = "I Bet You Look Good on the Dancefloor"
        ws['T2'].value = "R U Mine?"
        ws['U2'].value = "Do I Wanna Know?"
        ws['V2'].value = "Four Out Of Five"
        ws['W2'].value = "Crying Lightning"
        ws['X2'].value = "Cornerstone"
        ws['Y2'].value = "Pretty Visitors"
        ws['Z2'].value = "Alex Turner"
        ws['AA2'].value = "Nicholas O'Malley"
        ws['AB2'].value = "Matt Helders"
        ws['AC2'].value = "Jamie Cook"
        ws['AJ2'].value="image link"

        wb.save("test.xlsx")

        d1 = getBands("test.xlsx")
        d2 = [{'Band Name': 'Arctic Monkeys', 'Place of Origin': 'Sheffield, England', 'Year Started': '2002', 'Genre': 'Rock', 'Website': 'https://www.arcticmonkeys.com/', 'Albums': ["Whatever People Say I Am, That's What I'm Not", 'Favourite Worst Nightmare', 'Humbug', 'Suck It and See', 'AM', 'Tranquility Base Hotel and Casino'],
               'Members': ['Alex Turner', "Nicholas O'Malley", 'Matt Helders', 'Jamie Cook'], 'Notable Songs': ['Brianstorm', 'Star Treatment', '505', 'I Bet You Look Good on the Dancefloor', 'R U Mine?', 'Do I Wanna Know?', 'Four Out Of Five', 'Crying Lightning', 'Cornerstone', 'Pretty Visitors'], 'Image':"image link"}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getBands_2(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Band Name"
        ws['B1'].value = "Place of Origin"
        ws['C1'].value = "Year Started"
        ws['D1'].value = "Genre"
        ws['E1'].value = "Website"
        for i in range(1, 11):
            ws[chr(69+i)+'1'].value = "Album "+str(i)
        for i in range(1, 11):
            ws[chr(79+i)+'1'].value = "Notable Song "+str(i)
        ws['Z1'].value = "Member 1"
        for i in range(2, 11):
            ws['A'+chr(63+i)+'1'].value = "Member "+str(i)
        ws['AJ1'].value = "Image"

        ws['A2'].value = "Arctic Monkeys"
        ws['B2'].value = "Sheffield, England"
        ws['C2'].value = "2002"
        ws['D2'].value = "Rock"
        ws['E2'].value = "https://www.arcticmonkeys.com/"
        ws['F2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['G2'].value = "Favourite Worst Nightmare"
        ws['H2'].value = "Humbug"
        ws['I2'].value = "Suck It and See"
        ws['J2'].value = "AM"
        ws['K2'].value = "Tranquility Base Hotel and Casino"
        ws['P2'].value = "Brianstorm"
        ws['Q2'].value = "Star Treatment"
        ws['R2'].value = "505"
        ws['S2'].value = "I Bet You Look Good on the Dancefloor"
        ws['T2'].value = "R U Mine?"
        ws['U2'].value = "Do I Wanna Know?"
        ws['V2'].value = "Four Out Of Five"
        ws['W2'].value = "Crying Lightning"
        ws['X2'].value = "Cornerstone"
        ws['Y2'].value = "Pretty Visitors"
        ws['Z2'].value = "Alex Turner"
        ws['AA2'].value = "Nicholas O'Malley"
        ws['AB2'].value = "Matt Helders"
        ws['AC2'].value = "Jamie Cook"
        ws['AJ2'].value = "image link"

        ws['A3'].value = "Franz Ferdinand"
        ws['B3'].value = "Glasgow, Scotland"
        ws['C3'].value = "2002"
        ws['D3'].value = "Rock"
        ws['E3'].value = "https://franzferdinand.com/"
        ws['F3'].value = "Franz Ferdinand"
        ws['G3'].value = "You Could Have It So Much Better"
        ws['H3'].value = "Tonight: Franz Ferdinand"
        ws['I3'].value = "Right Thoughts, Right Words, Right Actions"
        ws['J3'].value = "Always Ascending"
        ws['P3'].value = "Take Me Out"
        ws['Q3'].value = "The Fallen"
        ws['R3'].value = "Glimpse of Love"
        ws['S3'].value = "Love Illumination"
        ws['T3'].value = "This Fire"
        ws['U3'].value = "Matinee"
        ws['V3'].value = "You're the Reason I'm Leaving"
        ws['W3'].value = "Bullet"
        ws['X3'].value = "Walk Away"
        ws['Y3'].value = "Do You Want To"
        ws['Z3'].value = "Alex Kapranos"
        ws['AA3'].value = "Paul Thomson"
        ws['AB3'].value = "Bob Hardy"
        ws['AC3'].value = "Julian Corrie"
        ws['AD3'].value = "Gino Bardot"
        ws['AJ3'].value = "image link"


        wb.save("test.xlsx")

        d1 = getBands("test.xlsx")
        d2 = [{'Band Name': 'Arctic Monkeys', 'Place of Origin': 'Sheffield, England', 'Year Started': '2002', 'Genre': 'Rock', 'Website': 'https://www.arcticmonkeys.com/', 'Albums': ["Whatever People Say I Am, That's What I'm Not", 'Favourite Worst Nightmare', 'Humbug', 'Suck It and See', 'AM', 'Tranquility Base Hotel and Casino'], 'Members': ['Alex Turner', "Nicholas O'Malley", 'Matt Helders', 'Jamie Cook'], 'Notable Songs': ['Brianstorm', 'Star Treatment', '505', 'I Bet You Look Good on the Dancefloor', 'R U Mine?', 'Do I Wanna Know?', 'Four Out Of Five', 'Crying Lightning', 'Cornerstone', 'Pretty Visitors'],'Image':"image link"},
              {'Band Name': 'Franz Ferdinand', 'Place of Origin': 'Glasgow, Scotland', 'Year Started': '2002', 'Genre': 'Rock', 'Website': 'https://franzferdinand.com/', 'Albums': ['Franz Ferdinand', 'You Could Have It So Much Better', 'Tonight: Franz Ferdinand', 'Right Thoughts, Right Words, Right Actions', 'Always Ascending'], 'Members': ['Alex Kapranos', 'Paul Thomson', 'Bob Hardy', 'Julian Corrie', 'Gino Bardot'], 'Notable Songs': ['Take Me Out', 'The Fallen', 'Glimpse of Love', 'Love Illumination', 'This Fire', 'Matinee', "You're the Reason I'm Leaving", 'Bullet', 'Walk Away', 'Do You Want To'],'Image':"image link"}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getBands_3(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Band Name"
        ws['B1'].value = "Place of Origin"
        ws['C1'].value = "Year Started"
        ws['D1'].value = "Genre"
        ws['E1'].value = "Website"
        for i in range(1, 11):
            ws[chr(69+i)+'1'].value = "Album "+str(i)
        for i in range(1, 11):
            ws[chr(79+i)+'1'].value = "Notable Song "+str(i)
        ws['Z1'].value = "Member 1"
        for i in range(2, 11):
            ws['A'+chr(63+i)+'1'].value = "Member "+str(i)
        ws['AJ1'].value = "Image"

        ws['A2'].value = "Arctic Monkeys"
        ws['B2'].value = "Sheffield, England"
        ws['C2'].value = "2002"
        ws['D2'].value = "Rock"
        ws['E2'].value = "https://www.arcticmonkeys.com/"
        ws['F2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['G2'].value = "Favourite Worst Nightmare"
        ws['H2'].value = "Humbug"
        ws['I2'].value = "Suck It and See"
        ws['J2'].value = "AM"
        ws['K2'].value = "Tranquility Base Hotel and Casino"
        ws['P2'].value = "Brianstorm"
        ws['Q2'].value = "Star Treatment"
        ws['R2'].value = "505"
        ws['S2'].value = "I Bet You Look Good on the Dancefloor"
        ws['T2'].value = "R U Mine?"
        ws['U2'].value = "Do I Wanna Know?"
        ws['V2'].value = "Four Out Of Five"
        ws['W2'].value = "Crying Lightning"
        ws['X2'].value = "Cornerstone"
        ws['Y2'].value = "Pretty Visitors"
        ws['Z2'].value = "Alex Turner"
        ws['AA2'].value = "Nicholas O'Malley"
        ws['AB2'].value = "Matt Helders"
        ws['AC2'].value = "Jamie Cook"
        ws['AJ2'].value = "image link"


        ws['A3'].value = "Franz Ferdinand"
        ws['B3'].value = "Glasgow, Scotland"
        ws['C3'].value = "2002"
        ws['D3'].value = "Rock"
        ws['E3'].value = "https://franzferdinand.com/"
        ws['F3'].value = "Franz Ferdinand"
        ws['G3'].value = "You Could Have It So Much Better"
        ws['H3'].value = "Tonight: Franz Ferdinand"
        ws['I3'].value = "Right Thoughts, Right Words, Right Actions"
        ws['J3'].value = "Always Ascending"
        ws['P3'].value = "Take Me Out"
        ws['Q3'].value = "The Fallen"
        ws['R3'].value = "Glimpse of Love"
        ws['S3'].value = "Love Illumination"
        ws['T3'].value = "This Fire"
        ws['U3'].value = "Matinee"
        ws['V3'].value = "You're the Reason I'm Leaving"
        ws['W3'].value = "Bullet"
        ws['X3'].value = "Walk Away"
        ws['Y3'].value = "Do You Want To"
        ws['Z3'].value = "Alex Kapranos"
        ws['AA3'].value = "Paul Thomson"
        ws['AB3'].value = "Bob Hardy"
        ws['AC3'].value = "Julian Corrie"
        ws['AD3'].value = "Gino Bardot"
        ws['AJ3'].value = "image link"


        ws['A4'].value = "Royal Blood"
        ws['B4'].value = "Brighton, England"
        ws['C4'].value = "2013"
        ws['D4'].value = "Rock"
        ws['E4'].value = "https://royalbloodband.com/"
        ws['F4'].value = "Royal Blood"
        ws['G4'].value = "How Did We Get So Dark?"
        ws['P4'].value = "Figure It Out"
        ws['Q4'].value = "Lights Out"
        ws['R4'].value = "I Only Lie When I Love You"
        ws['S4'].value = "Little Monster"
        ws['T4'].value = "Out Of The Black"
        ws['U4'].value = "Ten Tonne Skeleton"
        ws['V4'].value = "Come On Over"
        ws['W4'].value = "Hook, Line & Sinker"
        ws['X4'].value = "You Can Be So Cruel"
        ws['Y4'].value = "Loose Change"
        ws['Z4'].value = "Ben Thatcher"
        ws['AA4'].value = "Mike Kerr"
        ws['AJ4'].value = "image link"


        wb.save("test.xlsx")

        d1 = getBands("test.xlsx")
        d2 = [{'Band Name': 'Arctic Monkeys', 'Place of Origin': 'Sheffield, England', 'Year Started': '2002', 'Genre': 'Rock', 'Website': 'https://www.arcticmonkeys.com/', 'Albums': ["Whatever People Say I Am, That's What I'm Not", 'Favourite Worst Nightmare', 'Humbug', 'Suck It and See', 'AM', 'Tranquility Base Hotel and Casino'], 'Members': ['Alex Turner', "Nicholas O'Malley", 'Matt Helders', 'Jamie Cook'], 'Notable Songs': ['Brianstorm', 'Star Treatment', '505', 'I Bet You Look Good on the Dancefloor', 'R U Mine?', 'Do I Wanna Know?', 'Four Out Of Five', 'Crying Lightning', 'Cornerstone', 'Pretty Visitors'],'Image':"image link"}, {'Band Name': 'Franz Ferdinand', 'Place of Origin': 'Glasgow, Scotland', 'Year Started': '2002', 'Genre': 'Rock', 'Website': 'https://franzferdinand.com/', 'Albums': ['Franz Ferdinand', 'You Could Have It So Much Better',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   'Tonight: Franz Ferdinand', 'Right Thoughts, Right Words, Right Actions', 'Always Ascending'], 'Members': ['Alex Kapranos', 'Paul Thomson', 'Bob Hardy', 'Julian Corrie', 'Gino Bardot'], 'Notable Songs': ['Take Me Out', 'The Fallen', 'Glimpse of Love', 'Love Illumination', 'This Fire', 'Matinee', "You're the Reason I'm Leaving", 'Bullet', 'Walk Away', 'Do You Want To'],'Image':"image link"}, {'Band Name': 'Royal Blood', 'Place of Origin': 'Brighton, England', 'Year Started': '2013', 'Genre': 'Rock', 'Website': 'https://royalbloodband.com/', 'Albums': ['Royal Blood', 'How Did We Get So Dark?'], 'Members': ['Ben Thatcher', 'Mike Kerr'], 'Notable Songs': ['Figure It Out', 'Lights Out', 'I Only Lie When I Love You', 'Little Monster', 'Out Of The Black', 'Ten Tonne Skeleton', 'Come On Over', 'Hook, Line & Sinker', 'You Can Be So Cruel', 'Loose Change'],'Image':"image link"}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    # testing getSongs()
    def test_getSongs_1(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Song Name"
        ws['B1'].value = "Album"
        ws['C1'].value = "Band"
        ws['D1'].value = "Year"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Language"
        ws['G1'].value = "Video"
        ws['H1'].value = "Lyrics"

        ws['A2'].value = "Brianstorm"
        ws['B2'].value = "Favourite Worst Nightmare"
        ws['C2'].value = "Arctic Monkeys"
        ws['D2'].value = "2007"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "English"
        ws['G2'].value = "https://www.youtube.com/watch?v=30w8DyEJ__0"
        ws['H2'].value = "https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html"

        wb.save("test.xlsx")

        d1 = getSongs("test.xlsx")
        d2 = [{'Song Name': 'Brianstorm', 'Album': 'Favourite Worst Nightmare', 'Band': 'Arctic Monkeys', 'Year': '2007', 'Genre': 'Indie Rock',
               'Language': 'English', 'Video': 'https://www.youtube.com/embed/30w8DyEJ__0', 'Lyrics': 'https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getSongs_2(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Song Name"
        ws['B1'].value = "Album"
        ws['C1'].value = "Band"
        ws['D1'].value = "Year"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Language"
        ws['G1'].value = "Video"
        ws['H1'].value = "Lyrics"

        ws['A2'].value = "Brianstorm"
        ws['B2'].value = "Favourite Worst Nightmare"
        ws['C2'].value = "Arctic Monkeys"
        ws['D2'].value = "2007"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "English"
        ws['G2'].value = "https://www.youtube.com/watch?v=30w8DyEJ__0"
        ws['H2'].value = "https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html"

        ws['A3'].value = "Take Me Out"
        ws['B3'].value = "Franz Ferdinand"
        ws['C3'].value = "Franz Ferdinand"
        ws['D3'].value = "2004"
        ws['E3'].value = "Indie Rock"
        ws['F3'].value = "English"
        ws['G3'].value = "https://www.youtube.com/watch?v=Ijk4j-r7qPA"
        ws['H3'].value = "https://www.azlyrics.com/lyrics/franzferdinand/takemeout.html"

        wb.save("test.xlsx")

        d1 = getSongs("test.xlsx")
        d2 = [{'Song Name': 'Brianstorm', 'Album': 'Favourite Worst Nightmare', 'Band': 'Arctic Monkeys', 'Year': '2007', 'Genre': 'Indie Rock', 'Language': 'English', 'Video': 'https://www.youtube.com/embed/30w8DyEJ__0', 'Lyrics': 'https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html'},
              {'Song Name': 'Take Me Out', 'Album': 'Franz Ferdinand', 'Band': 'Franz Ferdinand', 'Year': '2004', 'Genre': 'Indie Rock', 'Language': 'English', 'Video': 'https://www.youtube.com/embed/Ijk4j-r7qPA', 'Lyrics': 'https://www.azlyrics.com/lyrics/franzferdinand/takemeout.html'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getSongs_3(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Song Name"
        ws['B1'].value = "Album"
        ws['C1'].value = "Band"
        ws['D1'].value = "Year"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Language"
        ws['G1'].value = "Video"
        ws['H1'].value = "Lyrics"

        ws['A2'].value = "Brianstorm"
        ws['B2'].value = "Favourite Worst Nightmare"
        ws['C2'].value = "Arctic Monkeys"
        ws['D2'].value = "2007"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "English"
        ws['G2'].value = "https://www.youtube.com/watch?v=30w8DyEJ__0"
        ws['H2'].value = "https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html"

        ws['A3'].value = "Take Me Out"
        ws['B3'].value = "Franz Ferdinand"
        ws['C3'].value = "Franz Ferdinand"
        ws['D3'].value = "2004"
        ws['E3'].value = "Indie Rock"
        ws['F3'].value = "English"
        ws['G3'].value = "https://www.youtube.com/watch?v=Ijk4j-r7qPA"
        ws['H3'].value = "https://www.azlyrics.com/lyrics/franzferdinand/takemeout.html"

        ws['A4'].value = "Hook, Line & Sinker"
        ws['B4'].value = "How Did We Get So Dark?"
        ws['C4'].value = "Royal Blood"
        ws['D4'].value = "2017"
        ws['E4'].value = "Hard Rock"
        ws['F4'].value = "English"
        ws['G4'].value = "https://www.youtube.com/watch?v=FgrLHKM5z4E"
        ws['H4'].value = "https://www.azlyrics.com/lyrics/royalblood/hooklinesinker.html"

        wb.save("test.xlsx")

        d1 = getSongs("test.xlsx")
        d2 = [{'Song Name': 'Brianstorm', 'Album': 'Favourite Worst Nightmare', 'Band': 'Arctic Monkeys', 'Year': '2007', 'Genre': 'Indie Rock', 'Language': 'English', 'Video': 'https://www.youtube.com/embed/30w8DyEJ__0', 'Lyrics': 'https://www.azlyrics.com/lyrics/arcticmonkeys/brianstorm.html'}, {'Song Name': 'Take Me Out', 'Album': 'Franz Ferdinand', 'Band': 'Franz Ferdinand', 'Year': '2004', 'Genre': 'Indie Rock', 'Language': 'English',
                                                                                                                                                                                                                                                                                                               'Video': 'https://www.youtube.com/embed/Ijk4j-r7qPA', 'Lyrics': 'https://www.azlyrics.com/lyrics/franzferdinand/takemeout.html'}, {'Song Name': 'Hook, Line & Sinker', 'Album': 'How Did We Get So Dark?', 'Band': 'Royal Blood', 'Year': '2017', 'Genre': 'Hard Rock', 'Language': 'English', 'Video': 'https://www.youtube.com/embed/FgrLHKM5z4E', 'Lyrics': 'https://www.azlyrics.com/lyrics/royalblood/hooklinesinker.html'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    # testing getAlbums()
    def test_getAlbums_1(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Album Name"
        ws['B1'].value = "Band"
        ws['C1'].value = "Year Released"
        ws['D1'].value = "Highest Charting Song"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Number of Songs"
        ws['G1'].value = "Image"

        ws['A2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['B2'].value = "Arctic Monkeys"
        ws['C2'].value = "2006"
        ws['D2'].value = "I Bet You Look Good on the Dancefloor"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "13"
        ws['G2'].value = "https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg"

        wb.save("test.xlsx")

        d1 = getAlbums("test.xlsx")
        d2 = [{'Album Name': "Whatever People Say I Am, That's What I'm Not", 'Band': 'Arctic Monkeys', 'Year Released': '2006', 'Highest Charting Song': 'I Bet You Look Good on the Dancefloor',
               'Genre': 'Indie Rock', 'Number of Songs': 13, 'Image': 'https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getAlbums_2(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Album Name"
        ws['B1'].value = "Band"
        ws['C1'].value = "Year Released"
        ws['D1'].value = "Highest Charting Song"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Number of Songs"
        ws['G1'].value = "Image"

        ws['A2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['B2'].value = "Arctic Monkeys"
        ws['C2'].value = "2006"
        ws['D2'].value = "I Bet You Look Good on the Dancefloor"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "13"
        ws['G2'].value = "https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg"

        ws['A3'].value = "False Alarm"
        ws['B3'].value = "Two Door Cinema Club"
        ws['C3'].value = "2019"
        ws['D3'].value = "Talk"
        ws['E3'].value = "Pop"
        ws['F3'].value = "10"
        ws['G3'].value = "https://upload.wikimedia.org/wikipedia/en/1/17/Two_Door_Cinema_Club_-_False_Alarm_album_cover.jpg"

        wb.save("test.xlsx")

        d1 = getAlbums("test.xlsx")
        d2 = [{'Album Name': "Whatever People Say I Am, That's What I'm Not", 'Band': 'Arctic Monkeys', 'Year Released': '2006', 'Highest Charting Song': 'I Bet You Look Good on the Dancefloor', 'Genre': 'Indie Rock', 'Number of Songs': 13, 'Image': 'https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg'},
              {'Album Name': 'False Alarm', 'Band': 'Two Door Cinema Club', 'Year Released': '2019', 'Highest Charting Song': 'Talk', 'Genre': 'Pop', 'Number of Songs': 10, 'Image': 'https://upload.wikimedia.org/wikipedia/en/1/17/Two_Door_Cinema_Club_-_False_Alarm_album_cover.jpg'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)

    def test_getAlbums_3(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        ws['A1'].value = "Album Name"
        ws['B1'].value = "Band"
        ws['C1'].value = "Year Released"
        ws['D1'].value = "Highest Charting Song"
        ws['E1'].value = "Genre"
        ws['F1'].value = "Number of Songs"
        ws['G1'].value = "Image"

        ws['A2'].value = "Whatever People Say I Am, That's What I'm Not"
        ws['B2'].value = "Arctic Monkeys"
        ws['C2'].value = "2006"
        ws['D2'].value = "I Bet You Look Good on the Dancefloor"
        ws['E2'].value = "Indie Rock"
        ws['F2'].value = "13"
        ws['G2'].value = "https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg"

        ws['A3'].value = "False Alarm"
        ws['B3'].value = "Two Door Cinema Club"
        ws['C3'].value = "2019"
        ws['D3'].value = "Talk"
        ws['E3'].value = "Pop"
        ws['F3'].value = "10"
        ws['G3'].value = "https://upload.wikimedia.org/wikipedia/en/1/17/Two_Door_Cinema_Club_-_False_Alarm_album_cover.jpg"

        ws['A4'].value = "Always Ascending"
        ws['B4'].value = "Franz Ferdinand"
        ws['C4'].value = "2018"
        ws['D4'].value = "Always Ascending"
        ws['E4'].value = "Disco Rock"
        ws['F4'].value = "10"
        ws['G4'].value = "https://upload.wikimedia.org/wikipedia/en/0/01/Franz_Ferdinand_-_Always_Ascending_album_cover_art.png"

        wb.save("test.xlsx")

        d1 = getAlbums("test.xlsx")
        d2 = [{'Album Name': "Whatever People Say I Am, That's What I'm Not", 'Band': 'Arctic Monkeys', 'Year Released': '2006', 'Highest Charting Song': 'I Bet You Look Good on the Dancefloor', 'Genre': 'Indie Rock', 'Number of Songs': 13, 'Image': 'https://upload.wikimedia.org/wikipedia/en/5/5f/Whatever_People_Say_I_Am%2C_That%27s_What_I%27m_Not.jpg'}, {'Album Name': 'False Alarm', 'Band': 'Two Door Cinema Club', 'Year Released': '2019', 'Highest Charting Song': 'Talk',
                                                                                                                                                                                                                                                                                                                                                                          'Genre': 'Pop', 'Number of Songs': 10, 'Image': 'https://upload.wikimedia.org/wikipedia/en/1/17/Two_Door_Cinema_Club_-_False_Alarm_album_cover.jpg'}, {'Album Name': 'Always Ascending', 'Band': 'Franz Ferdinand', 'Year Released': '2018', 'Highest Charting Song': 'Always Ascending', 'Genre': 'Disco Rock', 'Number of Songs': 10, 'Image': 'https://upload.wikimedia.org/wikipedia/en/0/01/Franz_Ferdinand_-_Always_Ascending_album_cover_art.png'}]

        os.remove("test.xlsx")

        self.assertEqual(d1, d2)


if(__name__ == "__main__"):
    main()

#making Coverage Report:
#coverage run --source=database_jsonifier,test_database_jsonifier --branch test_database_jsonifier.py > coverage_report_database_jsonifier.txt 2>&1
#coverage report -m >> coverage_report_database_jsonifier.txt

#O/P of Coverage Report:
#cat coverage_report_database_jsonifier.txt
#...................
#----------------------------------------------------------------------
#Ran 19 tests in 0.944s
#
#OK
#Name                         Stmts   Miss Branch BrPart  Cover   Missing
#------------------------------------------------------------------------
#<<<<<<< HEAD
#database_jsonifier.py           81      9     46      1    92%   108-111, 116-119, 123, 122->123
#test_database_jsonifier.py     414      0     20      1    99%   518->exit
#-----------------------------------------------------------------------
#TOTAL                          495      9     66      2    98%
#=======
#database_jsonifier.py           85      9     50      1    93%   114-117, 122-125, 129, 128->129
#test_database_jsonifier.py     423      0     20      1    99%   531->exit
#------------------------------------------------------------------------
#TOTAL                          508      9     70      2    98%
#>>>>>>> 12728e646623b25157e58beb61375a96d45fb927

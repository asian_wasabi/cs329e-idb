from flask import Flask,render_template, request
#from models import app, db, Song, Album, Band
# from create_db import app, Song, Album, Band, create_songs, create_albums, create_bands
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import itertools
import random
from bs4 import BeautifulSoup, Comment
import requests

app=Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost/musicdb')
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost/musicdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

@app.route('/')
def index():
  return render_template("index.html")

@app.route('/about/')
def about():
  return render_template("about.html")

@app.route("/songs/")
def songs():
  songs = db.session.query(Song).all()
  return render_template("songs.html", songs = songs)

@app.route("/songs/<string:song_name>")
def specific_song(song_name):
  song_object=db.session.query(Song).filter_by(song_name=song_name).one()
  return render_template("specific_song.html",song_object=song_object,actual_lyrics_of_song=getLyrics(song_object.lyrics))

@app.route("/albums/")
def albums():
  albums = db.session.query(Album).all()
  return render_template("albums.html", albums = albums)

@app.route("/albums/<string:album_name>")
def specific_album(album_name):
  album_object = db.session.query(Album).filter_by(album_name=album_name).one()
  return render_template("specific_album.html",album_object=album_object)

@app.route("/bands/")
def bands():
  bands = db.session.query(Band).all()
  return render_template("bands.html", bands = bands)

@app.route("/bands/<string:band_name>")
def specific_band(band_name):
  band_object = db.session.query(Band).filter_by(band_name=band_name).one()
  return render_template("specific_band.html",band_object=band_object)

@app.route("/atlas/")
def atlas():
  places = db.session.query(Band.place_of_origin).all()  #returns a list of tuples, each tuple has only one element, so we can unpack each tuple and store in a set (we don't need or want duplicates)
  places=set(i[0] for i in places)
  return render_template("atlas.html",Places=list(places))

def getLyrics(url):
  response = requests.get(url)
  htmlfile = response.text
  parsed = BeautifulSoup(htmlfile, 'html.parser')
  answer=""
  for i in parsed.select("div"):
    comments = i.find_all(string=lambda text: isinstance(text, Comment))
    if comments == [' Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. ']:
      answer=answer+str(i)+"\n"
  return answer

class Song(db.Model):
    __tablename__ = 'songs'

    song_name = db.Column(db.String(100))
    album = db.Column(db.String(100))
    band = db.Column(db.String(100))
    year = db.Column(db.Integer)
    genre = db.Column(db.String(50))
    language = db.Column(db.String(10))
    video = db.Column(db.String(1000), primary_key = True)
    lyrics = db.Column(db.String(1000))

class Album(db.Model):
    __tablename__ = 'albums'

    album_name = db.Column(db.String(100), primary_key = True)
    band = db.Column(db.String(100))
    year_released = db.Column(db.Integer)
    highest_charting_song = db.Column(db.String(100))
    genre = db.Column(db.String(50))
    number_of_songs = db.Column(db.Integer)
    image = db.Column(db.String(1000))

class Band(db.Model):
    __tablename__ = 'bands'

    band_name = db.Column(db.String(100))
    place_of_origin = db.Column(db.String(100))
    year_started = db.Column(db.Integer)
    genre = db.Column(db.String(50))
    website = db.Column(db.String(1000), primary_key = True)
    albums = db.Column(db.ARRAY(db.String(100)))
    notable_songs = db.Column(db.ARRAY(db.String(100)))
    members = db.Column(db.ARRAY(db.String(1000)))
    image = db.Column(db.String(1000))

db.create_all()

if(__name__=="__main__"):
    app.run(debug=True)
    #app.run()

Flask==1.0.2
gunicorn==19.9.0
Flask-SQLAlchemy==2.4.0
psycopg2==2.8.3
beautifulsoup4==4.7.1
openpyxl==2.6.2
requests==2.18.4

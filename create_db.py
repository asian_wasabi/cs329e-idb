# beginning of create_db.py
import json
from main import app, db, Song, Album, Band

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_songs():
    song = load_json('songs.json')

    for oneSong in song['Songs']:
        song_name = oneSong['Song Name']
        album = oneSong['Album']
        band = oneSong['Band']
        year = oneSong['Year']
        genre = oneSong['Genre']
        language = oneSong['Language']
        video = oneSong['Video']
        lyrics = oneSong['Lyrics']

        newSong = Song(song_name = song_name, album = album, band = band, year = year, genre = genre, language = language, video = video, lyrics = lyrics)
        
        db.session.add(newSong)
        # commit the session to my DB.
        db.session.commit()    

def create_albums():
    album = load_json('albums.json')

    for oneAlbum in album['Albums']:
        album_name = oneAlbum['Album Name']
        band = oneAlbum['Band']
        year_released = oneAlbum['Year Released']
        highest_charting_song = oneAlbum['Highest Charting Song']
        genre = oneAlbum['Genre']
        number_of_songs = oneAlbum['Number of Songs']
        image = oneAlbum['Image']

        newAlbum = Album(album_name = album_name, band = band, year_released = year_released, highest_charting_song = highest_charting_song, genre = genre, number_of_songs = number_of_songs, image = image)

        db.session.add(newAlbum)
        db.session.commit()
        
def create_bands():
    band = load_json('bands.json')
		
    for oneBand in band['Bands']:
        band_name = oneBand['Band Name']
        place_of_origin = oneBand['Place of Origin']
        year_started = oneBand['Year Started']
        genre = oneBand['Genre']
        website = oneBand['Website']
        albums = oneBand['Albums']
        members = oneBand['Members']
        notable_songs = oneBand['Notable Songs']
        image = oneBand['Image']

        newBand = Band(band_name = band_name, place_of_origin = place_of_origin, year_started = year_started, genre = genre, website = website, albums = albums, members = members, notable_songs = notable_songs, image = image)

        db.session.add(newBand)
        db.session.commit()

create_songs()
create_albums()
create_bands()
# end of create_db.py
